clear; 
clc;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parameters 
% 
a1 = 0.05;
a2 = 0.25;

b = 0.01;
c = b;

I = 0.1;

v1_0 = 0.1;
v2_0 = 0.1;

w1_0 = 0;
w2_0 = 0;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

d12_d21_array = [0.0, 0.0; 0.0, 0.2; -0.1, 0.2; -0.3, 0.2; -0.5, 0.2];



for n=1:size(d12_d21_array, 1)

    d12 = d12_d21_array(n, 1);
    d21 = d12_d21_array(n, 2);
    tspan = [0:0.5:100];
%     fprintf('using d12: %d, d21: %d\n', d12, d21)
%     options=odeset('RelTol',10^-10,'AbsTol', 10^-10);
%     [T, result] = ode15s(@(t, var) fitzhugh(t, var, a1, a2, b, c, I, d12, d21), tspan, [v1_0 v2_0 w1_0 w2_0], options);
    [T, result] = ode15s(@(t, var) fitzhugh(t, var, a1, a2, b, c, I, d12, d21), tspan, [v1_0 w1_0 v2_0 w2_0]);
    
    s = sprintf('A%d.dat', n + 10);
    A = [result(:,1) result(:,3) result(:,2) result(:,4)];
    save(s, 'A', '-ascii')
end
