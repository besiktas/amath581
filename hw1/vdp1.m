function dydt = vdp1(t, y, epsilon)
% VDP1 Summary of this function goes here
%  dydt = [y(2); (-y(1) - epsilon * (y(1)^2 + 1) * y(2))];
dydt = [y(2); epsilon * (1-y(1)^2)*y(2)-y(1)];

 
