% problem 2


clear; 
clc;


% initial conditions
y_t0 = sqrt(3);
dy_dt0 = 1;


tstart = 0;
tend = 32;
dt = 0.5;

tspan = [tstart:dt:tend];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% part A
% answers into three columns of A7 responding to different epsilon
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

A7 = zeros(length(tspan), 3);

epsilon = [0.1, 1, 20];
for n = 1:length(epsilon)
    eps = epsilon(n);
    [t,y] = ode45(@(t, y) vdp1(t, y, eps), tspan, [y_t0; dy_dt0]);
   
    % add y(t) to A7 columns 
    
    A7(:, n) = y(:,1);
end

save A7.dat A7 -ascii

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% part B 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
y_t0 = 2;
dy_dt0 = pi^2; 

% create tolerance array 
TOL_array = [10^-4, 10^-5, 10^-6, 10^-7, 10^-8, 10^-9, 10^-10];

tspan = [0 32];


%%%%%%%%%%%%%%%%
%%%%%%%% ODE45
%%%%%%%%%%%%%%%%
local_truncation_order = zeros(1, length(TOL_array));



for n=1:length(TOL_array)
    TOL = TOL_array(n);
    options = odeset('AbsTol',TOL,'RelTol',TOL);
    
    [T, Y] = ode45(@(t, y) vdp1(t, y, 1), tspan, [y_t0, dy_dt0], options);

    local_truncation_order(n) = mean(diff(T));
end

slope = polyfit(log(local_truncation_order), log(TOL_array), 1);
slope = slope(1);

save A8.dat slope -ascii

%%%%%%%%%%%%%%%%
%%%%%%%% ODE23
%%%%%%%%%%%%%%%%
local_truncation_order = zeros(1, length(TOL_array));



for n=1:length(TOL_array)
    TOL = TOL_array(n);
    options = odeset('AbsTol',TOL,'RelTol',TOL);
    
    [T, Y] = ode23(@(t, y) vdp1(t, y, 1), tspan, [y_t0, dy_dt0], options);

    local_truncation_order(n) = mean(diff(T));
end

slope = polyfit(log(local_truncation_order), log(TOL_array), 1);
slope = slope(1);

save A9.dat slope -ascii

%%%%%%%%%%%%%%%%
%%%%%%%% ODE113
%%%%%%%%%%%%%%%%
local_truncation_order = zeros(1, length(TOL_array));



for n=1:length(TOL_array)
    TOL = TOL_array(n);
    options = odeset('AbsTol',TOL,'RelTol',TOL);
    
    [T, Y] = ode113(@(t, y) vdp1(t, y, 1), tspan, [y_t0, dy_dt0], options);

    local_truncation_order(n) = mean(diff(T));
end

slope = polyfit(log(local_truncation_order), log(TOL_array), 1);
slope = slope(1);



save A10.dat slope -ascii
