% problem 1

clear;
clc;

%
% ic, y(t = 0) = pi/sqrt(2)
ic = pi/sqrt(2);
f = @(t, yn) -3 * yn * sin(t);
y_t_true = @(t) (pi * exp(3 * (cos(t) - 1)))/sqrt(2);

% anonymous error function error functio
error_func = @(y_true, y_num) mean(abs(y_true - y_num));


% part (a)

delta_time_array = [2^-2, 2^-3, 2^-4, 2^-5, 2^-6, 2^-7, 2^-8];

error_values = zeros(1, length(delta_time_array));
for i=1:length(delta_time_array)
    
    % get delta T, and t_span to create y_true and y_n
    dt = delta_time_array(i);
    t_span = 0:dt:5;
   
    y_n = zeros(length(t_span), 1);
    y_true = zeros(length(t_span), 1);
    
    y_n(1) = ic;
    y_true(1) = y_t_true(0);
    
    for t_n = 1:length(y_n) - 1
        
        t = t_span(t_n);
        yn = y_n(t_n);
        
        y_n(t_n + 1) = y_n(t_n) + dt * f(t, yn);
        y_true(t_n + 1) = y_t_true(t_span(t_n + 1));
    end
    error_values(i) = mean(abs(y_n - y_true));        
end
% plot(log(delta_time_array), log(error_values))
slope = polyfit(log(delta_time_array), log(error_values), 1);
slope = slope(1);

save A1.dat y_n -ascii
save A2.dat error_values -ascii
save A3.dat slope -ascii

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% part B


error_values = zeros(1, length(delta_time_array));
for i=1:length(delta_time_array)
    
    % get delta T, and t_span to create y_true and y_n
    dt = delta_time_array(i);
    t_span = 0:dt:5;
   
    y_n = zeros(length(t_span), 1);
    y_true = zeros(length(t_span), 1);
    
    y_n(1) = ic;
    y_true(1) = y_t_true(0);
    
    for t_n = 1:length(y_n) - 1
        
        t = t_span(t_n);
        yn = y_n(t_n);
        
        % heuns method 
        y_n(t_n + 1) = y_n(t_n) + (dt/2) * (f(t, yn) + f(t + dt, yn + dt*f(t, yn)));
        y_true(t_n + 1) = y_t_true(t_span(t_n + 1));
    end
    error_values(i) = mean(abs(y_n - y_true));        
end

% plot(log(delta_time_array), log(error_values))
slope = polyfit(log(delta_time_array), log(error_values), 1);
slope = slope(1);

save A4.dat y_true -ascii
save A5.dat error_values -ascii
save A6.dat slope -ascii