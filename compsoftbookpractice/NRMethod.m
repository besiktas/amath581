classdef NRMethod
    properties 
        initial_condition
        f_x
        f1_x
        true_root
        iterations
    end
    
    methods
        function obj = NRMethod(initial_condition, func)
            % intial condition for some x
            obj.initial_condition = initial_condition;
            obj.iterations = [initial_condition];
            
            % create the symbolic part of f(x) 
            % for example f(x) = -x - cos(x)
            syms x;
            obj.f_x = str2func(func);
            obj.f1_x = matlabFunction( diff(obj.f_x(x)));
                
            obj.true_root = solve(0 == sym(obj.f_x), x);
        end
        
%         function true_root = get_true_root(obj, val)
%             syms x
%             eqn = val == sym(obj.f_x);
%             true_root = solve(eqn, x)
%             obj.true_root = true_root
%         end
    
        function x_n1 = get_x_n1(obj, x_n)
            x_n1 = vpa(x_n - obj.f_x(x_n)/obj.f1_x(x_n));
        end
        
        function obj = iterate(obj)
            xn = obj.iterations(length(obj.iterations));
            x_n1 = get_x_n1(obj, xn);
            obj.iterations = [obj.iterations x_n1];
        end
            
    end
end
