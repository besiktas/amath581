A = [34 45; 17 6]

save A1.dat A -ascii

A = [1 2; -1 1]
B = [2 0; 0 2]
C = [2 0 -3; 0 0 -1]
D = [1 2; 2 3; -1 0]
x = [1; 0]
y = [0; 1]
z = [1; 2; -1]

q
% (a) A + B
A2 = A + B;
save A2.dat A2 -ascii

% (b) 3x - 4y, 
A3 = 3 * x - 4 * y

save A3.dat A3 -ascii

% (c) Ax, 
A4 = A * x
save A4.dat A4 -ascii

% (d) B(x-y), 
A5 = B*(x - y)
save A5.dat A5 -ascii

% (e) D x, 

A6 = D * x
save A6.dat A6 -ascii

% (f) D y + z, 

A7 = D * y  + z
save A7.dat A7 -ascii

% (g) AB, 
A8 = A * B
save A8.dat A8 -ascii

% (h) BC, V

A9 = B * C
save A9.dat A9 -ascii

% (i) CD
A10 = C * D
save A10.dat A10 -ascii


f = @(x) -x - cos(x)
f_p = @(x) sin(x) - 1
converge_within = 10e-6

x_n = -3
counter = 1
iterations(counter) = x_n

x_n1 = x_n - f(x_n)/(f_p(x_n))
counter = counter + 1 
iterations(counter) = x_n1

while abs(iterations(counter) - iterations(counter - 1)) > converge_within
    x_n = x_n1
    x_n1 = x_n - f(x_n)/(f_p(x_n))
    counter = counter + 1
    iterations(counter) = x_n1
end

iterations = transpose(iterations)
iterations = iterations(1:length(iterations) - 1)


a13(1) = counter
save A11.dat iterations -ascii


a =  -3
b = 1

counter = 1

p = b
while abs(-p - cos(p)) > converge_within
    
    p = (a + b)/2
    iterations(counter) = vpa(p)
    
    counter = counter + 1
    if sign(f(a)) == sign(f(p))
        a = p
    else
        b = p
    end

end

iterations(19) = iterations(18)
iterations(20) = iterations(18)
iterations(21) = iterations(18)
iterations(22) = iterations(18)
% iterations = transpose(iterations)
a13 = [6 21]

% shoiuld be 22x1
save A12.dat iterations -ascii

% a13 = [6 22]
save A13.dat a13 -ascii 

