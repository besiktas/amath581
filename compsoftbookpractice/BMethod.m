classdef BMethod
    properties 
        x_a
        x_b
        f_x
        f1_x
        true_root
        iterations
    end
    
    methods
        function obj = BMethod(x_a, x_b, func)
            % intial condition for some x
            obj.x_a = x_a;
            obj.x_b = x_b;
            obj.iterations = [];
            
            % create the symbolic part of f(x) 
            % for example f(x) = -x - cos(x)
            syms x;
            obj.f_x = str2func(func);
            obj.f1_x = matlabFunction( diff(obj.f_x(x)));
                
            % true root just b/c 
            obj.true_root = solve(0 == sym(obj.f_x), x);
        end
        
        function x_n1 = get_x_n1(obj, x_n)
            x_n1 = vpa(x_n - obj.f_x(x_n)/obj.f1_x(x_n));
        end
        
        function obj = iterate(obj)
            xn = obj.iterations(length(obj.iterations));
            x_n1 = get_x_n1(obj, xn);
            obj.iterations = [obj.iterations x_n1];
        end
            
    end
end
