

% x(1) = −3
x = -3
% f(x) = -x - cos(x)
% 
num_iterations = 0

guesses = []
% newton raphson method
% soln of x apprx = .001531441


curr_f_x = f_x
converge_cond = -(-0.74) - cos(-0.74) 
converge_within = 10^-6
while abs(converge_cond - curr_fx) > converge_within:
    num_iterations = num_iterations + 1

    f_x  = - (x_n) - cos(x_n)
    f_dx = - 1  + sin(x_n)
    x_n_1 = x_n - f_x / f_dx
    guesses = [guesses; x_n_1]
    x_n = x_n_1
end

