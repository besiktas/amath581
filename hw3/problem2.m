clear all;
clc;

fmat = load('Fmat.mat');
pvec = load('permvec.mat');

fmat = fmat.Fmat;
pvec = pvec.permvec;

center_matrix = fmat(161:240, 161:240);
center_matrix_idxs = zeros(16, 2);

permuted_matrix = zeros(size(center_matrix));
idx = 1;
for i=1:4
    for j=1:4
        column_idx = i * 20 - 19;
        row_idx = j * 20 - 19;
        center_matrix_idxs(idx, :) = [row_idx column_idx];
        idx = idx + 1;
    end
end

idx = 1;
for i=1:4
    for j=1:4
        perm_matrix_to_use = pvec(idx);
        idx_to_use = center_matrix_idxs(perm_matrix_to_use, :);
        row_idx = idx_to_use(1);
        col_idx = idx_to_use(2);
        submatrix_to_use = center_matrix(row_idx:row_idx+19, col_idx:col_idx+19);
        
        new_matrix_idx = center_matrix_idxs(idx, :);
        permuted_matrix(new_matrix_idx(1):new_matrix_idx(1)+19, new_matrix_idx(2):new_matrix_idx(2)+19) = submatrix_to_use;
        
        idx = idx + 1;
    end
end

new_fmat = fmat;
new_fmat(161:240, 161:240) = permuted_matrix;
A4 = abs(new_fmat);
save('A4.dat', 'A4', '-ascii');
new_fmat = ifftshift(new_fmat);
new_fmat = ifft2(new_fmat);
% imshow(uint8(abs(new_fmat)));
new_fmat= abs(new_fmat);


save('A5.dat', 'new_fmat', '-ascii');


