clc;
clear all;

%% parameters
L = 10;
m = 8;
n = m*m;
z = zeros(n);
dx = (L - (-L))/m;
dy = dx;

%%  Matrix A
% $$ A = \partial^{2}_{x} + \partial^{2}_{y} $$

% Ad matrix
A_d = zeros(n);
dd = ones(n,1)*-4; 
A_d = diag(dd);

% Ay matrix
e1 = ones(n,1); 
e0 = zeros(n,1);

ey = zeros(m,1);
ey(1) = 1;
ey = repmat(ey, m, 1);
A_y = spdiags([ey e1 e1 ey], [-m, -1 1 m], n, n);

% A_y = spdiags([e1 e1],[-1 1],n,n);
% A_y(1,m) = 1; 
% A_y(m,1)=1;
% for j=1:m-1
%   A_y(m*j+1,(j+1)*m)=1;
%   A_y((j+1)*m,m*j+1)=1;
%   A_y((m*j+1),(m*j))=0;
%   A_y((m*j),(m*j+1))=0;
% end 
A_y=full(A_y);

% Ax matrix
Ax=spdiags([e1 e1 e1 e1], [-((m-1)*m) -m m ((m-1)*m)], n, n);
Ax=full(Ax);

A=(A_d+A_y+Ax)/(dx^2);
% spy(matA);

A = full(A);
save('A1.dat', 'A', '-ascii');

%%  Matrix B
B = spdiags([e1 -e1 e1 -e1],[-(m*(m-1)) -(m) (m) (m*(m-1))],n,n)/(2*dx);
B = full(B);
save('A2.dat', 'B', '-ascii');

%%  Matrix C
% $$ \partial_y $$
%

C=spdiags([-e1 e1], [-1 1], n, n); 
C(1, m) = -1; 
C(m, 1) =  1;

for j=1:m-1
 C(m*j+1,(j+1)*m) = -1;
 C((j+1)*m,m*j+1) = 1;
 C((m*j+1),(m*j)) = 0;
 C((m*j),(m*j+1)) = 0;
end 
C=full(C)/(2*dx);

C = full(C);
save('A3.dat', 'C', '-ascii');
