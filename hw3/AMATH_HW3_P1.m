%% AMATH Homework 3 P1 
clear all
close all 
clc 

dx=20/8;
m=8;
n=m^2;

%Creating Ad
Ad=zeros(n);
d=ones(n,1)*-4; % Row Vector 
Ad=diag(d);

%Creating Ay
e1=ones(n,1); %ones vector 
e0=zeros(n,1); %zero vector
Ay=spdiags([e1 e1],[-1 1],n,n);
Ay(1,m)=1; Ay(m,1)=1;
for j=1:m-1
 Ay(m*j+1,(j+1)*m)=1;
 Ay((j+1)*m,m*j+1)=1;
 Ay((m*j+1),(m*j))=0;
 Ay((m*j),(m*j+1))=0;
end 
Ay=full(Ay);

%Creating Ax
Ax=spdiags([e1 e1 e1 e1],[-(7*m) -m m (7*m)],n,n);
Ax=full(Ax);

%A matrix 
matA=(Ad+Ay+Ax)/(dx^2);
spy(matA);

%% B matrix 
matB=spdiags([e1 -e1 e1 -e1],[-(m*(m-1)) -(m) (m) (m*(m-1))],n,n)/(2*dx); %PHIX
matB=full(matB);
spy(matB);

%% C matrix 
matC=spdiags([-e1 e1],[-1 1],n,n); %Diagonal inputs %PHIY
matC(1,m)=-1; matC(m,1)=1; %Periodic conditions

for j=1:m-1
 matC(m*j+1,(j+1)*m)=1;
 matC((j+1)*m,m*j+1)=-1;
 matC((m*j+1),(m*j))=0;
 matC((m*j),(m*j+1))=0;
end 
matC=full(matC)/(2*dx);
% spy(matC)

%% Problem 2 
% %Defining Known
% cd('/Users/justiceofosu/Documents/UW Grad School /Fall 2018/AMATH 581 /Homework 3');
% load('Fmat'); %Loading images 
% load('permvec'); %Loading images
% 
% % imshow(Fmat) %Viewing image
% 
% center_mat=Fmat(161:240,161:240); %Center Matrix 
% new_center_matrix=zeros(4,4);
% for i=1:16 
%     permvec(i) 
% end 








