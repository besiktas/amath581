import numpy as np

import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3
import matplotlib.animation as animation

from main import main

# https://stackoverflow.com/questions/51512141/how-to-make-matplotlib-saved-gif-looping

soln, params, matrix = main()

data = []
for k in soln.y.T:
    data.append(k.reshape(params.n, params.n, order='F'))

fig = plt.figure()

ims = []
for i in data:
    im = plt.imshow(i)
    ims.append([im])

ani = animation.ArtistAnimation(fig, ims, interval=500, blit=True,  repeat_delay=500)
ani.save('test.gif', writer='imagemagick')



# def create_lineimage(soln, gif_name='newest.gif', params=defaultP):
#     fig = plt.figure()
#     data = []
#     for k in soln.y.T:
#         data.append(k.reshape(params.n, params.n, order='F'))

#     ims = []
#     for i in data:
#         im = plt.pcolor(i)
#         ims.append([im])

#     ani = animation.ArtistAnimation(
#         fig, ims, interval=500, blit=True, repeat_delay=500)
#     ani.save(gif_name, writer='imagemagick')
