classdef create_matrices
    %CREATE_MATRICES Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        L
        M
        N
        nu
        
        xyspan
        DX
        DY
        
        % A specific properties
        A_diagonal
        A_11

        % B specific properties
        
        % C specific properties
        
        % Vorticities
    end
    
%     methods(Static)
%         function A = create_A(n)
%             A = [n n];
%         end
%     end
    
    methods
        function obj = create_matrices(L, M, nu)
            %CREATE_MATRICES Construct an instance of this class
            %   Detailed explanation goes here
            obj.L = L;
            obj.M = M;
            obj.N = M * M;
            obj.nu = nu;
            obj.A_diagonal = -4;
            xyspan = linspace(-obj.L, obj.L, obj.M+1);
            obj.xyspan = xyspan(1:end-1);

            obj.DX = xyspan(2) - xyspan(1);
            obj.DY = dx;
            obj.A_11 = 2;
            
            
        end
        
        function A = create_A(obj)

            %% METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            
            %% A_d
            dd = obj.A_diagonal * ones(obj.N, 1); 
            A_d = zeros(obj.N);
            A_d = spdiags(dd, [0], obj.N, obj.N);

            %% A_y matrix
            e1 = ones(obj.N, 1); 
            e0 = zeros(obj.N, 1);

            ey = zeros(obj.M, 1);
            ey(1) = 1;
            ey = repmat(ey, obj.M, 1);

            A_y = spdiags([e1 e1],[-1 1], obj.N, obj.N);
            A_y(1,m) = 1; 
            A_y(m,1)=1;
            for j=1:m-1
              A_y(obj.M*j+1,(j+1)*obj.M)=1;
              A_y((j+1)*obj.M,obj.M*j+1)=1;
              A_y((obj.M*j+1),(obj.M*j))=0;
              A_y((obj.M*j),(obj.M*j+1))=0;
            end 

            % Ax matrix
            A_x = spdiags([e1 e1 e1 e1], [-((obj.M-1)*obj.M) -obj.M ((obj.M-1)*obj.M)], obj.N, obj.N);

            A=A_d+A_y+A_x;
            A(1,1) = obj.A_11;
            A=A/(obj.DX^2);
        end
    end
end

