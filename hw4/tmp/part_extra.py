# Generated with SMOP  0.41
from libsmop import *
# part_extra.m

    clc
    clear('all')
    ## SAVING AND DISPLAYING PARAMS

    config=struct()
# part_extra.m:6
    config.save_output = copy(1)
# part_extra.m:8

    config.plot_images = copy(0)
# part_extra.m:9
    config.print_time = copy(1)
# part_extra.m:10

    config.extra_credit = copy(1)
# part_extra.m:11
    ## parameters
    L=10
# part_extra.m:13

    m=64
# part_extra.m:14

    n=dot(m,m)
# part_extra.m:15
    # z = zeros(n);
    nu=0.001
# part_extra.m:17
    tspan=arange(0,4,0.5)
# part_extra.m:18
    guassian_bump_param_y=20
# part_extra.m:19
    # xyspan discretization
    xyspan=linspace(- L,L,m + 1)
# part_extra.m:22
    xyspan=xyspan(arange(1,end() - 1))
# part_extra.m:23
    dx=xyspan(2) - xyspan(1)
# part_extra.m:25
    dy=copy(dx)
# part_extra.m:26
    # maybe ode options
# TOL = 10^-2;
# ode_options = odeset('AbsTol',TOL,'RelTol',TOL);

    ## variable checking
    if dx != ((L - (- L)) / m):
        error('dx and dy issue.  Should equal one another')

    ## create the vorticity matrix
    X,Y=meshgrid(xyspan,xyspan,nargout=2)
# part_extra.m:39
    omega_0=exp(- (X ** 2) - ((Y ** 2) / guassian_bump_param_y))
# part_extra.m:40
    # turn into column vector
# can use omega_col or just reshape(vec, m
    omega_0=reshape(omega_0,[],1)
# part_extra.m:44
    ##  Matrix A

    # A_d matrix
# dd = ones(n,1)*-4;
# A_d = spdiags(dd, 0, n, n);
#
# # A_y matrix
# e1 = ones(n,1);
# ey = zeros(m,1);
# ey(1) = 1;
# ey = repmat(ey, m, 1);
# A_y = spdiags([ey e1 e1 ey], [-m, -1 1 m], n, n);
#
# # A_x matrix
# A_x=spdiags([e1 e1 e1 e1], [-((m-1)*m) -m m ((m-1)*m)], n, n);
#
# A=A_d+A_y+A_x;
# A(1,1) = 2;
# A=A/(dx^2);
### ABOVE IS PREV

    dd=ones(n,1)
# part_extra.m:67
    A_d=spdiags(dot(- 4,dd),concat([0]),n,n)
# part_extra.m:68
    # Ay matrix
    e1=ones(n,1)
# part_extra.m:71
    e0=zeros(n,1)
# part_extra.m:72
    ey=zeros(m,1)
# part_extra.m:74
    ey[1]=1
# part_extra.m:75
    ey=repmat(ey,m,1)
# part_extra.m:76
    # A_y2 = spdiags([ey e1 e1 ey], [-m, -1 1 m], n, n);

    A_y=spdiags(concat([e1,e1]),concat([- 1,1]),n,n)
# part_extra.m:79
    A_y[1,m]=1
# part_extra.m:80
    A_y[m,1]=1
# part_extra.m:81
    for j in arange(1,m - 1).reshape(-1):
        A_y[dot(m,j) + 1,dot((j + 1),m)]=1
# part_extra.m:83
        A_y[dot((j + 1),m),dot(m,j) + 1]=1
# part_extra.m:84
        A_y[(dot(m,j) + 1),(dot(m,j))]=0
# part_extra.m:85
        A_y[(dot(m,j)),(dot(m,j) + 1)]=0
# part_extra.m:86

    # Ax matrix
    A_x=spdiags(concat([e1,e1,e1,e1]),concat([- (dot((m - 1),m)),- m,m(dot((m - 1),m))]),n,n)
# part_extra.m:90
    A=A_d + A_y + A_x
# part_extra.m:92
    A[1,1]=2
# part_extra.m:93
    A=A / (dx ** 2)
# part_extra.m:94
    ##  Matrix B
    B=spdiags(concat([e1,- e1,e1,- e1]),concat([- (dot(m,(m - 1))),- (m),(m),(dot(m,(m - 1)))]),n,n)
# part_extra.m:98
    B=B / (dot(2,dx))
# part_extra.m:99
    ##  Matrix C
# ec = ones(m,1);
# ec(end) = 0;
# ec = repmat(ec, m, 1);
#
# C = spdiags([ey -ec ec ey], [-m -1 1 m], n, n);
# C(n-m+1,n) = -1;
# C(n,n-m+1) = 1;
# C = C/(2*dx);
#
# C from prev hw
    C=spdiags(concat([- e1,e1]),concat([- 1,1]),n,n)
# part_extra.m:112
    C[1,m]=- 1
# part_extra.m:113
    C[m,1]=1
# part_extra.m:114    for j in arange(1,m - 1).reshape(-1):
        C[dot(m,j) + 1,dot((j + 1),m)]=- 1
# part_extra.m:117
        C[dot((j + 1),m),dot(m,j) + 1]=1
# part_extra.m:118
        C[(dot(m,j) + 1),(dot(m,j))]=0
# part_extra.m:119
        C[(dot(m,j)),(dot(m,j) + 1)]=0
# part_extra.m:120

    C=C / (dot(2,dx))
# part_extra.m:122
    ## Using dummy struct to pass vars around for rhs eqn
    dummy=struct('nu',nu,'m',m)
# part_extra.m:126
    ##