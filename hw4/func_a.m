function dwdt = func_a(t, omega, part, A, B, C, dummy)
switch part
    case 1
        psi = A \ omega;
    case 2
        y = dummy.L\(dummy.P * omega);
        psi = dummy.U \ y;
    case 3
        omega_f = reshape(omega, dummy.m, dummy.m);
        omega_fft = fft2(omega_f);
        omega = reshape(ifft2(omega_fft), [], 1);
        % dummy.K is KX.^2 + KY.^2 with KX KY from meshgrid with
        psi_fft = -omega_fft ./ dummy.K;

        psi = real(ifft2(psi_fft));
        psi = reshape(psi, [], 1);
    case 4
        [psi, flag, relres, iter, resvec] = bicgstab(A, omega, dummy.TOL, dummy.ITER);
    case 5
        [psi, flag] = gmres(A, omega, dummy.RESTARTS, dummy.TOL, dummy.ITER);
end
% size(psi)
% t
dwdt = ((-B * psi) .* (C * omega)) + ((C * psi).*(B * omega)) + (dummy.nu * A * omega);
end