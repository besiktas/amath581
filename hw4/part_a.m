clc;
clear all;

%% SAVING AND DISPLAYING PARAMS

config = struct();

config.save_output=1; % save outputs
config.plot_images=0;
config.print_time=1; % time various methods
config.extra_credit=1;
%% parameters
L = 10; %spatial domain is [-L, L] for x,y
m = 64; %number of discretization points
n = m*m;
% z = zeros(n);
nu = 0.001;
tspan = 0:0.5:4;
guassian_bump_param_y = 20;

% xyspan discretization
xyspan = linspace(-L, L, m+1);
xyspan = xyspan(1:end-1);

dx = xyspan(2) - xyspan(1);
dy = dx;

% maybe ode options
% TOL = 10^-2;
% ode_options = odeset('AbsTol',TOL,'RelTol',TOL);


%% variable checking
if dx ~= ((L - (-L))/m)
    error("dx and dy issue.  Should equal one another")
end

%% create the vorticity matrix
[X, Y] = meshgrid(xyspan, xyspan);
omega_0 = exp(-(X.^2) - ((Y.^2)./guassian_bump_param_y));

% turn into column vector
% can use omega_col or just reshape(vec, m
omega_0 = reshape(omega_0, [], 1);

%%  Matrix A

A_d = zeros(n);
dd = ones(n,1)*-4;
A_d = spdiags(dd, [0], n, n);

% Ay matrix
e1 = ones(n,1);
e0 = zeros(n,1);

ey = zeros(m,1);
ey(1) = 1;
ey = repmat(ey, m, 1);
% A_y2 = spdiags([ey e1 e1 ey], [-m, -1 1 m], n, n);

A_y = spdiags([e1 e1],[-1 1],n,n);
A_y(1,m) = 1;
A_y(m,1)=1;
for j=1:m-1
  A_y(m*j+1,(j+1)*m)=1;
  A_y((j+1)*m,m*j+1)=1;
  A_y((m*j+1),(m*j))=0;
  A_y((m*j),(m*j+1))=0;
end

% Ax matrix
A_x=spdiags([e1 e1 e1 e1], [-((m-1)*m) -m m ((m-1)*m)], n, n);

A=A_d+A_y+A_x;
A(1,1) = 2;
A=A/(dx^2);


%%  Matrix B
B = spdiags([e1 -e1 e1 -e1],[-(m*(m-1)) -(m) (m) (m*(m-1))],n,n);
B = B/(2*dx);

%%  Matrix C

C=spdiags([-e1 e1], [-1 1], n, n);
C(1, m) = -1;
C(m, 1) =  1;

for j=1:m-1
 C(m*j+1,(j+1)*m) = -1;
 C((j+1)*m,m*j+1) = 1;
 C((m*j+1),(m*j)) = 0;
 C((m*j),(m*j+1)) = 0;
end
C=C/(2*dx);

%% Using dummy struct to pass vars around for rhs eqn
dummy = struct('nu', nu, 'm', m);

%% LU Decompisition related stuff
[dummy.L, dummy.U, dummy.P] = lu(A);

%% FFT related stuff
L_fft = L * 2; % since my params use [-L:L] and not [-L/2:L/2]
kx=(2*pi/L_fft)*[0:(m/2-1) (-m/2):-1];
kx(1)=10^-6;
ky = kx;
[KX, KY] = meshgrid(kx, ky);
dummy.K = KX.^2 + KY.^2;


%% solving part A, B
% modes correspond to
% 1 => A\w
% 2 => LU
% 3 => FFT/IFFT
modes = [2];


for part=modes

    % struct is somewhat like hashtable
    if config.print_time
        disp(['doing part: ', num2str(part)]);
        tic
    end


    [t, output] = ode45(@(t, y) func_a(t, y, part, A, B, C, dummy), tspan, omega_0);
    % print time
    if config.print_time
        toc
    end

    % plotting
    if config.plot_images
        disp(['plotting image for part: ', num2str(part)])
    end

    % save output for compsoftbook
    if config.save_output
        output_filename = sprintf('A%d.dat', part);
        save(output_filename, 'output', '-ascii');
    end
end
