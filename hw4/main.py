from types import SimpleNamespace
import random

# numpy + scipy
import numpy as np
from scipy.sparse import spdiags, linalg
from scipy.integrate import solve_ivp
from scipy.stats import invgauss

# plotting libs
import matplotlib.pyplot as plt
from matplotlib import animation

plt.style.use("fivethirtyeight")


class MatrixParameters(SimpleNamespace):
    """docstring for Params"""

    def __init__(self, L=10, n=64, nu=0.001, tspan=(0, 5), tstepsize=0.5):
        self.L = L
        self.n = n
        self.nn = n * n
        self.nu = nu
        self.tstepsize = tstepsize
        self.tspan = tspan
        self.t_eval = np.arange(tspan[0], tspan[1] + tstepsize, tstepsize)
        # create xyspan with periodic conditions™
        self.xyspan = np.linspace(-self.L, self.L, self.n + 1)[0:-1]
        self.dx = self.xyspan[1] - self.xyspan[0]


class ImageParameters(SimpleNamespace):
    def __init__(self, title='default', format_='gif', cmap='twilight'):
        self.title = title
        self.format_ = format_

        if self.format_ == 'gif':
            self.writer = 'imagemagick'

        self.filename = f'{self.title}.{self.format_}'
        self.cmap = cmap


defaultP = MatrixParameters()
defaultImageP = ImageParameters()


def create_vorticity_matrix(xyspan,
                            x0y0=[0, 0],
                            xbump=1.0,
                            ybump=1.0,
                            charge=1.0,
                            strength=1.0):
    X, Y = np.meshgrid(xyspan, xyspan)
    Xpart = (((X - x0y0[0]) ** 2) / xbump)
    Ypart = (((Y - x0y0[1]) ** 2) / ybump)
    vorticity_vals = - Xpart - Ypart
    # vorticity_vals = - (((X - x0y0[0]) ** 2) / xbump) - \
    # (((Y - x0y0[1]) ** 2) / ybump)
    omega = charge * strength * np.exp(vorticity_vals)
    return omega


def get_vorticity(params=defaultP, opts=None):
    omega_0 = create_vorticity_matrix(params.xyspan, ybump=20)
    return omega_0.reshape(-1, order='F')


def create_A(n, dx, A_11=2, diagonal_constant=-4):
    nn = n * n
    dd = diagonal_constant * np.ones(nn)
    e1 = np.ones(nn)

    A_d = spdiags(dd, [0], nn, nn)

    A_x_diags = [-((n - 1) * n), -n, n, ((n - 1) * n)]
    A_x = spdiags([e1, e1, e1, e1], A_x_diags, nn, nn)

    A_y = spdiags([e1, e1], [-1, 1], nn, nn)
    # need this to iterate through
    A_y = A_y.tolil()

    for j in range(0, n - 1):
        A_y[n * j, (j + 1) * n - 1] = 1
        A_y[(j + 1) * n - 1, n * j] = 1
        A_y[(j + 1) * n, (j + 1) * n - 1] = 0
        A_y[(j + 1) * n - 1, (j + 1) * n] = 0

    A_y[nn - 1, -n] = 1
    A_y[nn - n, -1] = 1

    A = A_d + A_x + A_y
    A[0, 0] = A_11

    A = A / (dx**2)
    return A.tocsc()


def create_B(n, dx):
    nn = n ** 2
    e1 = np.ones(nn)
    B_diags = [-(n * (n - 1)), -n, n, (n * (n - 1))]

    B = spdiags([e1, -e1, e1, -e1], B_diags, nn, nn)
    B = B / (2 * dx)
    return B.tocsc()


def create_C(n, dx):
    nn = n ** 2
    e1 = np.ones(nn)
    C = spdiags([-e1, e1], [-1, 1], nn, nn)
    C = C.tolil()

    for j in range(0, n - 1):
        C[n * j, (j + 1) * n - 1] = -1
        C[(j + 1) * n - 1, n * j] = 1
        C[(j + 1) * n, (j + 1) * n - 1] = 0
        C[(j + 1) * n - 1, (j + 1) * n] = 0

    C[nn - 1, -n] = 1
    C[nn - n, -1] = 1
    C = C / (2 * dx)
    return C.tocsc()


def fft_create_laplacian(params=defaultP):
    L_fft = params.L * 2
    kx = (2 * np.pi / L_fft) * np.array(list(range(0, int(params.n / 2))) + list(range(int(-params.n / 2), -1 + 1)))
    kx[0] = 10**-6
    ky = kx
    KX, KY = np.meshgrid(kx, ky)
    K = np.square(KX) + np.square(KY)
    return K


def create_matrices(n, dx):
    A = create_A(n, dx)
    B = create_B(n, dx)
    C = create_C(n, dx)
    K = fft_create_laplacian()

    return {'A': A, 'B': B, 'C': C, 'K': K}


def lu_method(omega_t, matrix, params):
    lu = linalg.splu(matrix['A'])
    psi = lu.solve(omega_t)

    return psi


def fft_method(omega_t, matrix, params):
    omega_f = np.reshape(omega_t, (params.n, params.n), order='F')
    omega_fft = np.fft.fft2(omega_f)
    omega = np.reshape(np.fft.ifft2(omega_fft), -1, order='F')
    psi_fft = np.divide(-omega_fft, matrix['K'])
    psi = np.real(np.fft.ifft2(psi_fft))
    psi = np.reshape(psi, -1, order='F')
    return psi


psi_solving_methods = {
    'lu': lu_method,
    'fft': fft_method
}

def rhs(t, omega_t, matrix, psi_method, params=defaultP):
    """
    rhs to use for ode45/solve_ivp equation

    """

    # psi = psi_solving_methods[psi_method](omega_t, matrix, params)
    lu = linalg.splu(matrix['A'].tocsc())
    psi = lu.solve(omega_t)

    dwdt = ((-matrix['B'] @ psi) * (matrix['C'] @ omega_t)) + \
        ((matrix['C'] @ psi) * (matrix['B'] * omega_t)) + \
        (params.nu * matrix['A'] * omega_t)
    return dwdt

def solve_for_omega(omega_col, matrix, params=defaultP, psi_method='lu'):
    soln = solve_ivp(lambda t, y: rhs(t, y, matrix, psi_method, params),
                     params.tspan, omega_col, t_eval=params.t_eval,
                     vectorized=False)
    return soln



def plot_k(soln, params=defaultP, k=0):
    y = soln.y.T[k].reshape([params.n, params.n], order='F')
    plt.imshow(y)
    plt.show()

def create_simple_image_for_soln(soln,
                                 image_params=defaultImageP,
                                 params=defaultP):

    fig, ax = plt.subplots(1)
    ax.set_yticklabels([])
    ax.set_xticklabels([])

    if image_params.title:
        ax.set_title(image_params.title, loc='right')

    data = []
    for k in soln.y.T:
        data.append(k.reshape(params.n, params.n, order='F'))

    ims = []
    for i in data:
        # im = plt.pcolormesh(i)
        im = plt.pcolormesh(i, cmap=image_params.cmap, shading='gouraud')
        ims.append([im])

    ani = animation.ArtistAnimation(
        fig, ims, interval=50, blit=True, repeat_delay=200)

    if image_params.format_ == 'gif':
        ani.save(image_params.filename, writer='imagemagick')
    else:
        ani.save(image_params.filename)

    plt.close(fig)


def random_point(l):
    x0 = random.randint(-l, l)
    y0 = random.randint(-l, l)
    return (x0, y0)


def random_charge(s=[-1, 1]):
    return random.choice(s)


def two_opposite_charged(matrix, params=defaultP):
    """
        Two oppositely “charged” Gaussian vortices next to each other,
        i.e. one with positive amplitude, the other with negative amplitude.
    """

    point1 = create_vorticity_matrix(
        params.xyspan, x0y0=(-1, 0), ybump=10, charge=1.0)

    point2 = create_vorticity_matrix(
        params.xyspan, x0y0=(1, 0), ybump=10, charge=-1.0)

    omega_0 = point1 + point2
    omega_0 = omega_0.reshape(-1, order='F')

    soln = solve_for_omega(omega_0, matrix)

    image_params = ImageParameters(title='two "opposite" charged')

    create_simple_image_for_soln(soln, image_params)


def two_same_charged(matrix, params=defaultP):
    """
        Two same “charged” Gaussian vortices next to each other.
    """

    point1 = create_vorticity_matrix(
        params.xyspan, x0y0=(-1.5, 0), ybump=1, charge=1.0)
    point2 = create_vorticity_matrix(
        params.xyspan, x0y0=(1.5, 0), ybump=1, charge=1.0)

    omega_0 = point1 + point2
    omega_0 = omega_0.reshape(-1, order='F')

    soln = solve_for_omega(omega_0, matrix)
    # , cmap='cool'
    image_params = ImageParameters(title='two "same" charged')
    create_simple_image_for_soln(soln, image_params)


def two_pairs_diff_charges(matrix, params=defaultP):
    """
    Two pairs of oppositely “charged” vortices which can be made to collide
    with each other.
    """

    point1a = create_vorticity_matrix(
        params.xyspan, x0y0=(-1, -1), xbump=10, ybump=10, charge=1.0)
    point1b = create_vorticity_matrix(
        params.xyspan, x0y0=(1, 1), xbump=10, ybump=10, charge=1.0)
    point2a = create_vorticity_matrix(
        params.xyspan, x0y0=(-1, 1), xbump=10, ybump=5, charge=-1.0)
    point2b = create_vorticity_matrix(
        params.xyspan, x0y0=(1, -1), xbump=10, ybump=5, charge=-1.0)

    omega_0 = point1a + point1b + point2a + point2b
    omega_0 = omega_0.reshape(-1, order='F')

    soln = solve_for_omega(omega_0, matrix)

    image_params = ImageParameters(title='two "pairs" opposite charged')
    create_simple_image_for_soln(soln, image_params)


def random_amount_of_charges(matrix, params=defaultP):
    """
        A random assortment (in position, strength, charge, ellipticity, etc.)
        of vortices on the periodic domain.
        Try 10-15 vortices and watch what happens.
        #Arguments function
    """
    point1 = random_point(params.L)
    omega_0 = create_vorticity_matrix(params.xyspan,
                                      x0y0=point1,
                                      charge=random_charge()
                                      )

    n = 10
    image_params = ImageParameters(title=f'{n} randomly charged')

    n -= 1
    while 0 < n:
        n -= 1
        new_point = random_point(params.L / 2)
        new_charge = random_charge()
        strength = invgauss.rvs(1, 0.1)
        # x_bump = np.random.pareto(10) * 100
        # y_bump = np.random.pareto(10) * 100
        x_bump = random.randint(1, 2)
        y_bump = random.randint(1, 2)

        omega_0 += create_vorticity_matrix(params.xyspan,
                                           x0y0=new_point,
                                           xbump=x_bump,
                                           ybump=y_bump,
                                           strength=strength,
                                           charge=new_charge)

    omega_0 = omega_0.reshape(-1, order='F')
    soln = solve_for_omega(omega_0, matrix)

    create_simple_image_for_soln(soln, image_params)


def main():
    params = MatrixParameters()
    matrix = create_matrices(n=params.n, dx=params.dx)
    return params, matrix


if __name__ == "__main__":

    # running whichever to make images
    # params, matrix = main()
    # two_opposite_charged(matrix, params)
    # two_same_charged(matrix, params)

    # two_pairs_diff_charges(matrix, params)

    # random_amount_of_charges(matrix, params)
    params = MatrixParameters()
    matrix = create_matrices(n=params.n, dx=params.dx)

    omega_0 = create_vorticity_matrix(params.xyspan, x0y0=(-1, 0), ybump=10, charge=1.0)
    omega_0 = omega_0.reshape(-1, order='F')

    # print('lu')
    # soln = solve_for_omega(omega_0, matrix, psi_method="lu")

    # print('fft')

    import timeit
    print('time for 10 fft: ', timeit.timeit('solve_for_omega(omega_0, matrix, psi_method="fft")', number=10, globals=globals()))
    print('time for 10 lu: ', timeit.timeit('solve_for_omega(omega_0, matrix, psi_method="lu")', number=10, globals=globals()))

    # soln = solve_for_omega(omega_0, matrix, psi_method="fft")

