function dwdt = func_b(omega_0, part, A, B, C, dummy)
%FUNC_B Summary of this function goes here
%   Detailed explanation goes here
omega_f = reshape(omega_0, dummy.m, dummy.m);
omega_fft = fft2(omega_f);

% dummy.K is KX.^2 + KY.^2 with KX KY from meshgrid with 
psi_fft = -omega_fft ./ dummy.K; 

psi = real(ifft2(psi_fft));
psi = reshape(psi, [], 1);

dwdt = ((-B * psi) .* (C * omega_0)) + ((C * psi).*(B * omega_0)) + (dummy.nu * A * omega_0);
end