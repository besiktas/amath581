%% NOTES PART A

% - set tspan, D1, D2, m, beta, L, n, X, Y
% - create the initial conditions u0 and v0
% - fft2(u0) and fft(v0)
% - reshape them to 4096x1, stack u0_vec on top of v0_vec, pass to RHS
% inside RHS:
% - take the first 4092 rows, reshape to 64x64 to get u_fft
% - next 4096 rows, reshape to 64x64 to get v_fft
% - ifft2(u_fft), ifft2(v_fft)
% - create matrices A, lambda and omega
% - take fft2 of each element that needs to be added together, divide the last element by K
% - reshape back to 4096x1, stack u on top of v
% use this RHS in ode45
% so in essence my rhs function returns a 8192x1 vector

% from eightD: So for your U equation, calculate the full nonlinear part( lambda(A)U - omega(A)V ) before you you fft
