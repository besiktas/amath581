clear all;
clc;

%% Init Params
L = 20;
L_ = L / 2;
n = 64;
nn = n^2;
Beta = 1;
D1 = 0.1;
D2 = D1;
tspan = 0:0.5:4;
x = linspace(-L/2, L/2, n + 1);
x = x(1:end-1);
y = x;

m=1;
[X, Y] = meshgrid(x, y);
u=tanh(sqrt(X.^2 + Y.^2)).*cos(m * angle(X + 1i * Y)-(sqrt(X.^2 + Y.^2)));
v=tanh(sqrt(X.^2 + Y.^2)).*sin(m * angle(X + 1i * Y)-(sqrt(X.^2 + Y.^2)));


%% anonymous functions 
% 
% Lambda = @(A) 1 - A;
% Omega = @(A) -Beta * A;

tic
%% Laplacian

kx = (2*pi/L)*[0:(n/2-1) (-n/2):-1];

ky = kx;
[KX, KY] = meshgrid(kx, ky);
K = KX.^2 + KY.^2;
K = sparse(-K);
K = reshape(K, [], 1);

%% Structure for part A
variablesA = struct('D1', D1, 'D2', D2, 'n', n, 'nn', nn, 'Beta', Beta);
variablesA.K = K;
variablesA.part = 'A';

variablesA.to_graph = 0;

%% Part A 
% putting u and v into fourier space
u = fft2(u);
v = fft2(v);

u = reshape(u, [], 1);
v = reshape(v, [], 1);
init_conditions = [u; v];

[t, output] = ode45(@(t, y) func_a_hw5(t, y, variablesA), tspan, init_conditions);

% output = fft2(output);
real_output = real(output);
imag_ouptut = imag(output);

save('A1.dat', 'real_output', '-ascii')
save('A2.dat', 'imag_ouptut', '-ascii')

% 



toc
%% Part B
tic
N = 30;
L = 20;
variablesB = struct('D1', D1, 'D2', D2, 'Beta', Beta);
variablesB.part = 'B';
variablesB.N = N;
variablesB.nn = nn;
variablesB.n = n;

[D, x] = cheb(N);

x=(L/2)*x;

D = D/(L/2);
D2 = D^2;
D2(1, :) = zeros(1, N+1);
D2(end, :) = zeros(1, N+1);

I=eye(length(D2));
L=kron(I,D2) + kron(D2,I);

variablesB.K = sparse(L);
variablesB.x = x;
variablesB.D = D;
variablesB.midpoint = 961;
y = x;

[X, Y] = meshgrid(x, y);

u = tanh(sqrt(X.^2 + Y.^2)).*cos(m * angle(X + 1i * Y)-(sqrt(X.^2 + Y.^2)));
v = tanh(sqrt(X.^2 + Y.^2)).*sin(m * angle(X + 1i * Y)-(sqrt(X.^2 + Y.^2)));
u = reshape(u, [], 1);
v = reshape(v, [], 1);

init_conditions = [u; v];

[t, output] = ode45(@(t, y) func_a_hw5(t, y, variablesB), tspan, init_conditions);
toc
save('A3.dat', 'output', '-ascii');
