function output = func_a_hw5(t, y, variables)
%RHS_HW5 Summary of this function goes here
%   Detailed explanation goes here
% U = -D1*Kvecfft2(U)vec + fft2(Nonlinear junk)vec
% V = -D2*Kvec*fft2(V)vec + fft2(Nonlinear junk)vec


%% unpacking of variables
D1 = variables.D1;
D2 = variables.D2;

% Lambda = variables.Lambda;
% Omega = variables.Omega;


%% main part 

if variables.part == 'A'
%     Kvec = variables.Kvec;
    %% part A
    
    % Kvec is from - K =  KX.^2 + KY.^2; Kvec=  reshape(K, [], 1);
    % Kvec = variables.Kvec;

    n = variables.n;
    nn = variables.nn;

    u_fft = y(1:nn);
    v_fft = y(nn+1:end);

    u = reshape(u_fft, n, n);
    v = reshape(v_fft, n, n);

    u = ifft2(u);
    v = ifft2(v);

    uvec = reshape(u, [], 1);
    vvec = reshape(v, [], 1);

    A = u.^2 + v.^2;

    dudt = reshape(fft2(Lambda(A) .* u - Omega(A)  .* v), [], 1) + D1 * variables.K .* u_fft;
    dvdt = reshape(fft2(Omega(A) .* u  + Lambda(A) .* v), [], 1) + D2 * variables.K .* v_fft;

    dudt = reshape(dudt, [], 1);
    dvdt = reshape(dvdt, [], 1);

    output = [dudt; dvdt];

elseif variables.part == 'B' 
    %% part b
    K = variables.K;
    midpoint = variables.midpoint;
    
    uvec = y(1:midpoint);
    vvec = y(midpoint+1:end);
    
    A = uvec.^2 + vvec.^2;
    
    dudt = (Lambda(A) .* uvec - Omega(A) .* vvec) + D1 * K * uvec;
    dvdt = (Omega(A) .* uvec + Lambda(A) .* vvec) + D2 * K * vvec;
    
    output = [dudt; dvdt];
    
end

end

%% functions used in func_a_hw5
function output = Lambda(A)
output = 1 - A;
end
% 
function output = Omega(A)
Beta = -1;
output = Beta * A;
end