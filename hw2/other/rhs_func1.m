function f = rhs_func1(x,y,K,eps)
    f1 = y(2);
    f2 = (K*x^2 - eps)*y(1);
    
    f = [f1;f2];
end