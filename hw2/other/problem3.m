clear all; %#ok
clc;


% PARAMETERS
gamma_vals = 0.05;
gamma_array = [gamma_vals, -gamma_vals];
dt = 0.1;
L = 2;
xspan=-L:dt:L;
tol = 10^(-4);

A_start = 1;
eps_start = 1;
K=1;


% use alternate method from slides
for modes=1:2
    eps = eps_start;
    d_eps = eps / 10;
    gamma = gamma_array(modes);
    A = A_start;
    for j=1:1000
        

        % -------------------------------------------------------------
        % QUESTION - should IC should based on new equation?
%             ic_2 = A*sqrt(gamma * abs(A)^2 + K*L^2 - eps);

        ic_1 = A;
        ic_2 = A*sqrt(gamma * abs(A)^2 + K*L^2 - eps);
        [t, y] = ode45(@(t, y) funcproblem3(t, y, K, gamma, eps), xspan, [ic_1 ic_2]);


        norm_phi = trapz(t, y(:,1).^2);

        %% A Shooting
        if abs(norm_phi - 1) < tol
            break
        else
            A = A/sqrt(norm_phi);
        end
            
        %% Epsilon Shooting 
        epscheck = y(end, 2) + sqrt(gamma * abs(y(end,1))^2 + K*L^2 - eps) * y(end, 1); 
%         epscheck = y(end, 2) + y(end, 1)*sqrt(K*L^2 - eps); 
        if abs(epscheck)<tol
            break;
        end
        
        if (-1)^(modes+1)*epscheck > 0
            eps = eps + d_eps;
        else
            eps = eps - d_eps/2;
            d_eps = d_eps/2;
        end
        
    end
    
    
    %% normalizing values
    norm_y = y(:,1) / sqrt(trapz(t, y(:,1).* y(:,1)));
   

    plot(t, norm_y); hold on;
    
    eigfuncs1 = y(:,1);
    eigfuncs2 = y(:,2);
    
    %%% QUESTION - what does eigvals correspond to here, since it would
    %%% have both epsilon and A as part of it?
    eigvals = [eps; A];
    
    norm_y1 = y(:,1)/sqrt(trapz(t, y(:,1) .* y(:,1)));
    norm_y2 = y(:,2)/sqrt(trapz(t, y(:,2) .* y(:,2)));
    
    if modes==1
        % A13 said it was correct on compsolve
        save('A13.dat', 'eigfuncs1', '-ascii');
        save('A14.dat', 'norm_y2', '-ascii');
        save('A15.dat', 'eigvals', '-ascii');
    else
        save('A16.dat', 'norm_y1', '-ascii');
        save('A17.dat', 'norm_y2', '-ascii');
        save('A18.dat', 'eigvals', '-ascii');
    end
end


% eigenfunctions 