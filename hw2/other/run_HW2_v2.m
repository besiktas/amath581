close all; clc; clear variables;

% set tolerance
tol = 10^-4;

% set parameters
L = 4;
xp = [-L:0.1:L];
K = 1;
A = 1;
eps_start = 0;
epsilon = ones(5,1);
phi = ones(81,5);

% iterate and solve
for mode = 1:5

    % set initial conditions
    eps = eps_start;
    deps = 1;
    yinit = [A; A*(K*L^2 - eps)^(1/2)];
    
    % Iterate ODE45
    for j = 1:1000
        [x,y] = ode45(@(x,y) rhs_func1(x,y,K,eps),xp,yinit);

        if abs(y(end,2) + y(end,1)*(K*L^2 - eps)^(1/2)) < tol
            epsilon(mode) = eps;
            break;
        end
        
        if (-1)^(mode+1)*y(end,2) > (-1)^(mode)*(y(end,1)*(K*L^2 - eps)^(1/2))
            eps = eps + deps;
        else
            eps = eps - deps/2;
            deps = deps/2;
        end        
        
    end
    
    eps_start = eps + .1;

     % Normalize
     norm = trapz(xp,y(:,1).*y(:,1)); % calculate the normalization
     %plot(xp,y(:,1)/sqrt(norm)); hold on % plot modes
     phi(:,mode) = y(:,1)/sqrt(norm);
     
end

V1 = abs(phi(:,1));
V2 = abs(phi(:,2));
V3 = abs(phi(:,3));
V4 = abs(phi(:,4));
V5 = abs(phi(:,5));

save A1.dat V1 -ascii
save A2.dat V2 -ascii
save A3.dat V3 -ascii
save A4.dat V4 -ascii
save A5.dat V5 -ascii
save A6.dat epsilon -ascii

% set tolerance
tol = 10^-4;

% set parameters
L = 4;
dx = 0.1;
xp = [-L:dx:L];
xn = length(xp);
K = 1;
A = 1;
eps_start = 0;
epsilon_1 = ones(1,5);
phi = ones(81,5);


% Create Matrix
Mat = zeros(length(xp));

% Define Boundaries
Mat(1,1) = (2/3) + dx^2*K*xp(1)^2;
Mat(1,2) = -(2/3);

Mat(xn,xn) = (2/3) + dx^2*K*xp(xn)^2;
Mat(xn,xn-1) = -(2/3);

% Define Interior
for j=2:xn-1
    Mat(j,j-1) = -1;
    Mat(j,j) = 2 + dx^2*K*xp(j)^2;
    Mat(j,j+1) = -1;
end

% Calculate Eigenvectors and Eigenvalues and sort
[V,D] = eig(Mat);
eps2a = zeros(81,1);
for j = 1:81
   eps2a(j,1) = D(j,j)/dx^2; 
end
[s,I] = sort(eps2a);

% Select Eigenvectors and Eigenvalues
EV = zeros(81,5);
eps2 = zeros(5,1);
for j=1:5
    EV(:,j) = V(:,I(j));
    eps2(j,1) = s(j);
end

% Plot
%figure(2)
for j = 1:5
    % Normalize
    norm = trapz(xp,EV(:,j).*EV(:,j)); % calculate the normalization
    %plot(xp,EV(:,j)/sqrt(norm)); hold on % plot modes
    phi2(:,j) = EV(:,j)/sqrt(norm);
end

V6 = abs(phi2(:,1));
V7 = abs(phi2(:,2));
V8 = abs(phi2(:,3));
V9 = abs(phi2(:,4));
V10 = abs(phi2(:,5));

save A7.dat V6 -ascii
save A8.dat V7 -ascii
save A9.dat V8 -ascii
save A10.dat V9 -ascii
save A11.dat V10 -ascii
save A12.dat eps2 -ascii

% set tolerance
tol = 10^-4;

% set parameters
L2 = 2;
xp3 = [-L2:0.1:L2];
K = 1;
A_start = 1;
eps_start = 2;
epsilon3_1 = ones(2,1);
epsilon3_2 = ones(2,1);
gam1 = 0.05;
gam2 = -0.05;

% iterate and solve
for mode = 1:2

    % set initial conditions
    A3 = A_start;
    eps = eps_start;
    deps = 1;
    
    % Iterate ODE45
        for k = 1:1000

            yinit = [A3; A3*(K*L2^2 - eps)^(1/2)];
            [x3a,y3a] = ode45(@(x,y) rhs_func2(x,y,gam1,K,eps),xp3,yinit);
            
            % Compute Norm
            norm = trapz(x3a, abs(y3a(:,1)));

            % Shoot for A
            if abs(norm - 1) < tol
                epsilon3_1(mode,1) = eps;
                break    
            else
                A3 = A3/norm^(1/2);
            end
        
            % Shoot for Epsilon
            if abs(y3a(end,2) - y3a(end,1)*(K*L2^2 - eps)^(1/2)) < tol
                epsilon3_1(mode,1) = eps;
                break;
            end
        
            if y3a(end,2) > (y3a(end,1)*(K*L2^2 - eps)^(1/2))
                eps = eps + deps;
            else
                eps = eps - deps/2;
                deps = deps/2;
            end    
        end
    
    eps_start = eps + .1;
end

V13 = abs(y3a(:,1));
V14 = abs(y3a(:,2));

save A13.dat V13 -ascii
save A14.dat V14 -ascii
save A15.dat epsilon3_1 -ascii

A_start = 1;
eps_start = 0;

% iterate and solve
for mode = 1:2

    % set initial conditions
    A3 = A_start;
    eps = eps_start;
    deps = 1;
    
    % Iterate ODE45
        for k = 1:1000

            yinit = [A3; A3*(gam2*A3^2 + K*L2^2 - eps)^(1/2)];
            [x3b,y3b] = ode45(@(x,y) rhs_func2(x,y,gam2,K,eps),xp3,yinit);
            
            % Compute Norm
            norm = trapz(xp3, abs(y3b(:,1)));

            % Shoot for A
            if abs(norm - 1) < tol
%                 epsilon3_2(mode,1) = eps;
%                 break    
            else
                A3 = A3/norm^(1/2);
            end
        
            % Shoot for Epsilon
            if abs(y3b(end,2) - y3b(end,1)*(K*L2^2 - eps)^(1/2)) < tol
                epsilon3_2(mode,1) = eps;
                break;
            end
        
            if (-1)^(mode)*y3b(end,2) > (-1)^(mode)*(y3b(end,1)*(K*L2^2 - eps)^(1/2))
                eps = eps + deps;
            else
                eps = eps - deps/2;
                deps = deps/2;
            end    
        
        end
    
    eps_start = eps + .1;
end

V16 = abs(y3b(:,1));
V17 = abs(y3b(:,2));

save A16.dat V17 -ascii
save A17.dat V16 -ascii
save A18.dat epsilon3_2 -ascii