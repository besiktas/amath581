function f = rhs_func2(x,y,gam,K,eps)
    f1 = y(2);
    f2 = (gam*(abs(y(1)))^2 + K*x^2* - eps)*y(1);
    
    f = [f1;f2];
end