function bc = bvp_bc(yl,yr,beta)
%BVP_BC Summary of this function goes here
%   Detailed explanation goes here

% f?(t) = [?3f(t) + 4f(t + ?t) ? f(t + 2?t)]/2?t  Forward
% f?(t) = [3f(t) ? 4f(t ? ?t) + f(t ? 2?t)]/2?t    Backward

% need 
% dt?
% f_t
% f_tm1, f_tm2
% f_tp1, f_tp2


% forward = (-3*f_t + 4*f_tp1 - f_tp2)/2dt
% backward= ( 3*f_t - 4*f_tm1 + f_tm2)/2dt
bc = [yl(1); yl(2)-1; yr(1)];

