function rhs = bvp_rhs(x, y, eps)
%RHS_BVP Summary of this function goes here
%   Detailed explanation goes here

% phi(x_i+1)
phi_x_i_p1  = y(1);

% phi(x_i)
phi_x_i     = y(2);

% phi(x_i-1)
phi_x_i_m1  = y(3);

rhs = (phi_x_i_p1 - 2*phi_x_i + phi_x_i_m1)/(dt^2) - (K * phi_x_i^2 - eps)*(phi_x_i);


