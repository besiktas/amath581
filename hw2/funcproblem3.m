function f = funcproblem3(x, y, K, gamma, eps)
%FUNCPROBLEM1 Summary of this function goes here
%   Detailed explanation goes here

% f(1) => phi_n(-L)    = A
% f(2) => phi_n(-L)/dx = A * sqrt(KL^2 - e_n)

f1 = y(2);
f2 = (gamma * abs(y(1))^2 + K * x^2 - eps) * y(1);

f = [f1; f2];

