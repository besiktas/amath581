clear all; %#ok
clc;

%%% parameters
L = 4;
K = 1; 
dt = 0.1;
xspan = -L:dt:L;
x0 = -L; 
x_n = 4;

% variables to randomly start with 
eps_start = 1;

% Loop to initialize interior of A (79x79)
A = zeros(79,79);

x1 = -L + dt;
A(1, 2) = -2/3;
A(1, 1) = 2/3 + dt^2 * K * x1^2 ;

for i=2:length(A)-1
    A(i, i-1) = -1;
    A(i, i+1) = -1;
    
    xi = x0 + (i)*dt;
    
    A(i, i) = 2 + dt^2 * K * xi^2;
end

% -------------------------------------------------------------------------
x79 = L - dt;

A(end, end-1) = -2/3;
A(end, end) = 2/3 + dt^2 * K * x79^2;


% Solving for eigenvalues/eigenfunctions
[V, D] = eig(A);


D = D/dt^2; % normalize 

[out, idx] = sort(diag(D)); 
sorted_eps = out(1:5);

for i=1:length(sorted_eps)
    
    ii = idx(i);
    
    eps = sorted_eps(i);
    phi_xm1 = V(end , ii);% phi(x_n-1)
    phi_xm2 = V(end - 1, ii);% phi(x_n-2)
    phi_xp1 = V(1 , ii)  ;% phi(x_1)
    phi_xp2 = V(1 + 1, ii)  ;% phi(x_2)
    
    phi_x0 = (4 * phi_xp1 - phi_xp2)/(3 + 2 * dt * sqrt(K*x0^2 - eps));
    % should xn be using x0 in sqrt() part?
    phi_xn = (4 * phi_xm1 - phi_xm2)/(3 + 2 * dt * sqrt(K*x0^2 - eps));
    
    phi_tmp = [phi_x0; V(:, ii); phi_xn];
    
%     plot(xspan,abs(phi_tmp)); hold on;
    % ---------------------------------------------------------------------
    %%% QUESTION - still not within tolerence and kelly said not to
    %%% normalize, so must be doing something wrong elsewhere?
    norm_phi = abs(phi_tmp)/sqrt(trapz(xspan, phi_tmp .* phi_tmp));
    norm_phi = abs(norm_phi);
    
    part_num = i+6;
    file_name = sprintf('A%d.dat', part_num);
    save(file_name, 'norm_phi', '-ascii');
end

% -------------------------------------------------------------------------
%%% QUESTION - compsoft said eps was incorrect, not sure why
save('A12.dat', 'sorted_eps', '-ascii');



   