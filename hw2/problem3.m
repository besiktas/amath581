clear all; %#ok
clc;
warning('off', 'MATLAB:ode45:IntegrationTolNotMet')

%% PARAMETERS
gamma_vals = 0.05;
gamma_array = [gamma_vals, -gamma_vals];
dt = 0.1;
L = 2;
K=1;
xspan=-L:dt:L;
tol = 10^(-4);

A_start = 10;
eps_start = .01;
d_eps_start = .01;

eigvals = zeros(2,1);

%% Loop To find A/Eps
% use alternate method from slides
for gamma_to_use=1:2
    gamma = gamma_array(gamma_to_use);
    eps_start = .01;
    d_eps_start = .01;
    A = A_start;
    for modes=1:2
        eps = eps_start;
        d_eps = d_eps_start;
        
        
        for k=1:1000
            % update IC each iteration
            ic_1 = A;
            ic_2 = A*sqrt(K*L^2 - eps);
            [t, y] = ode45(@(t, y) funcproblem3(t, y, K, gamma, eps), xspan, [ic_1 ic_2]);
            
            norm = trapz(t, y(:,1) .* y(:,1));
            phi_norm = abs(y(:,1))/sqrt(norm);
            
            epscheck = y(end, 2) + y(end, 1)*sqrt(K*L^2 - eps);
            %% A - Shooting
            if (abs(norm - 1) < tol) && (abs(epscheck)<tol)
                break
            else
                A = A/sqrt(norm);
            end
            
            %% Epsilon Shooting - kelly said use the same BC as in p1 in epscheck            
            if (-1)^(modes+1)*epscheck > 0
                eps = eps + d_eps;
            else
                eps = eps - d_eps/2;
                d_eps = d_eps/2;
            end
            
        end
        eigvals(modes) = eps;
        eps_start = eps + d_eps;
        
        if modes==1
            norm_y1 = abs(y(:,1))/sqrt(trapz(t, y(:,1) .* y(:,1)));
        else
            norm_y2 = abs(y(:,1))/sqrt(trapz(t, y(:,1) .* y(:,1)));
        end
    end
    
    %% normalizing values and saving
%     norm_y1 = abs(y(:,1))/sqrt(trapz(t, y(:,1) .* y(:,1)));
%     norm_y2 = abs(y(:,2))/sqrt(trapz(t, y(:,2) .* y(:,2)));
    
    if gamma_to_use==1
        save('A13.dat', 'norm_y1', '-ascii');
        save('A14.dat', 'norm_y2', '-ascii');
        save('A15.dat', 'eigvals', '-ascii');
    else
        save('A16.dat', 'norm_y1', '-ascii');
        save('A17.dat', 'norm_y2', '-ascii');
        save('A18.dat', 'eigvals', '-ascii');
    end
    plot(t, norm_y1); hold on;
    plot(t, norm_y2); hold on;
    legend();
    % plot looks like it is probably wrong, also warning about imaginary
    % parts of X and Y from ODE45
    
%     eigvals
end

%% 
% A14: .3434
% A17: .3412

