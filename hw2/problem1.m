close all;
clc;

%% parameters
L=4; 
K = 1;
dt = 0.1;
xspan =[-L:dt:L]; %#ok
tol = 10^-4;

eps_start = 0;

%% loop for 
eigenvaluesA6 = zeros(5,1);
for modes=1:5 
    A= 1;
    
    eps = eps_start;
%     d_eps = eps_start/10;
    d_eps = .1;
    
    for j=1:100
        
        [t, y] = ode45( @(t, y) funcproblem1(t, y, K, eps), xspan, [A A*sqrt(K*L^2 - eps)]);

        check = y(end, 2) + y(end, 1)*sqrt(K*L^2 - eps); 
        if abs(check)<tol
            break;
        end
        
        if (-1)^(modes+1)*check > 0
            eps = eps + d_eps;
        else
            eps = eps - d_eps/2;
            d_eps = d_eps/2;
        end
    end

    eps_start = eps + 0.1;
   
    %% normalizing and saving eigenfunctions 
    
    norm_y = abs(y(:,1)/sqrt(trapz(t, y(:,1).*y(:,1))));
    
    % should each norm_y we are saving correspond to the eps we saved?
    file_name = sprintf('A%d.dat', modes);
    save(file_name, 'norm_y', '-ascii');
    eigenvaluesA6(modes) = eps;
%     plot(t,norm_y); hold on;
end

%% saving eigenvalues 
save('A6.dat', 'eigenvaluesA6', '-ascii');