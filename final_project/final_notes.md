# computational notebook

<script src="//yihui.name/js/math-code.js"></script>
<script  src="//mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML"></script>

## topics
- Quantum Harmonic Oscillator 
- Vorticity-Streamfunction Equations 
- Reaction-Diffusion Systems 


Description:

```
(1) Write a “Computational Notebook” that summarizes, explains and investigates the numerical approaches, i.e., the theory, algorithms and Matlab implementation covered during the course. The aim of this project is that the notebook will serve you as a reference from now on to scientific computing and numerical approaches to differential equations. Organize the notebook to address three different major physical problems that we discussed during the course, e.g., Quantum Harmonic Oscillator, Vorticity-Streamfunction Equations, Reaction-Diffusion Systems (like a survey paper). See the lecture notes for additional problems (particularly at the end). Be sure to include graphical expositions of the findings and the results (suitable plots) and explain them thoroughly.
481: Make sure to include full derivations and analysis done in class, e.g. stability analysis, deriva- tion of central difference, etc (at least one for each topic).
581: In addition, pick a subject, and investigate it beyond the discussion in class/notes. Completeness of the discussion, usefulness of the notebook and coolness will be the major criteria for its evaluation.
```


# Harmonic Oscillators
While we specifically had a HW that discussed Quantum Harmonic Oscillators, Harmonic Oscillators in general are very applicable.  The difference between quantum harmonic oscillators and harmonic oscillators is in general:
_LIST REASONS_

Some applications of Harmonic Oscillators in ecology and biology are the "Lotka-Volterra model" equations.  These equations encompass are described as a predator prey model that will oscillate between predator-prey populations slowly increasing and then sharply decreasing.  An example of this can be shown in python below:
_Graph of Predator Prey model_

The derivation of this model (pg 178 from textbook)


HW2

Implementation in python.

# Reaction Diffision Systems


To do/Topics

Example from numerical methods in finance book (newton-raphson)

Differences between python and matlab indexing, scipy, etc


(1) Write a “Computational Notebook” that summarizes, explains and investigates the numerical approaches, i.e., the theory, algorithms and Matlab implementation covered during the course. The aim of this project is that the notebook will serve you as a reference from now on to scientific com- puting and numerical approaches to differential equations. Organize the notebook to address three different major physical problems that we discussed during the course, e.g., Quantum Harmonic Oscillator, Vorticity-Streamfunction Equations, Reaction-Diffusion Systems (like a survey paper). See the lecture notes for additional problems (particularly at the end). Be sure to include graph- ical expositions of the findings and the results (suitable plots) and explain them thoroughly.
481: Make sure to include full derivations and analysis done in class, e.g. stability analysis, deriva- tion of central difference, etc (at least one for each topic).
581: In addition, pick a subject, and investigate it beyond the discussion in class/notes. Completeness of the discussion, usefulness of the notebook and coolness will be the major criteria for its evaluation.
